﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    public class Program
    {
        static int[] arrmincol = new int[5];
        static int[] arrminrow = new int[5];
        static int[] p1 = new int[5];
        static int[] p2 = new int[50];
       static int[,] a1 = new int[5, 5] { { 100, 15, 6, 10, 9 }, { 15, 100, 5, 4, 8 }, { 6, 15, 100, 7, 9 }, { 10, 4, 7, 100, 11 }, { 9, 8, 9, 11, 100 } };
        static int[,] arrpos = new int[5, 5];
        static int min = 0;
        static int cost = 0;

        public static void Main(string[] args)
        {
            Program pg = new Program();
            int[,] a = new int[5, 5] { { 100, 15, 6, 10, 9 }, { 15, 100, 5, 4, 8 }, { 6, 15, 100, 7, 9 }, { 10, 4, 7, 100, 11 }, { 9, 8, 9, 11, 100 } };
           
            
            int[,] A = new int[5, 5];
            int[,] B = new int[5, 5];
            int i;
            int j;
            Console.WriteLine("TSP Problem");
            Console.WriteLine("Actual matrix is:");
            Console.WriteLine();
            for (i = 0; i < a.GetLength(0); i++)
            {
                for (j = 0; j < a.GetLength(0); j++)
                {
                    Console.Write(string.Format("{0}\t", a[i, j]));
                }

                Console.WriteLine();
            }


            pg.initialize(a);
            Console.WriteLine();
            a = pg.reduction(a);

            Console.ReadKey();
        }
        public void initialize(int[,] a)
        {

            int i = 0, j = 0;
            for (i = 0; i < a.GetLength(0); i++)
            {
                min = a[i, 0];
                for (j = 0; j < a.GetLength(1); j++)
                {
                    if (a[i, j] != 100)
                    {
                        if (a[i, j] < min)
                        {
                            min = a[i, j];
                        }
                    }
                }
                if (min == 100)
                {
                    arrminrow[i] = 0;
                }
                else
                {
                    arrminrow[i] = min;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Min value of 1st row is {0}", arrminrow[0]);
            Console.WriteLine("Min value of 2nd row is {0}", arrminrow[1]);
            Console.WriteLine("Min value of 3rd row is {0}", arrminrow[2]);
            Console.WriteLine("Min value of 4th row is {0}", arrminrow[3]);
            Console.WriteLine("Min value of 5th row is {0}", arrminrow[4]);
            Console.WriteLine();
            Console.WriteLine("Reduced row matrix is:");
            for (i = 0; i < a.GetLength(0); i++)
            {

                for (j = 0; j < a.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        a[i, j] = 100;
                    }
                    else
                    {
                        if (a[i, j] != 100)
                        {

                            a[i, j] = a[i, j] - arrminrow[i];
                        }
                    }
                    Console.Write(string.Format("{0}\t", a[i, j]));
                }
                Console.WriteLine();
            }
            for (j = 0; j < a.GetLength(0); j++)
            {
                min = a[0, j];
                for (i = 0; i < a.GetLength(1); i++)
                {
                    if (a[i, j] != 100)
                    {
                        if (a[i, j] < min)
                        {
                            min = a[i, j];
                        }
                    }
                }
                if (min == 100)
                {
                    arrminrow[j] = 0;
                }
                else
                {
                    arrmincol[j] = min;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Min value of 1st column is {0}", arrmincol[0]);
            Console.WriteLine("Min value of 2nd column is {0}", arrmincol[1]);
            Console.WriteLine("Min value of 3rd column is {0}", arrmincol[2]);
            Console.WriteLine("Min value of 4th column is {0}", arrmincol[3]);
            Console.WriteLine("Min value of 5th column is {0}", arrmincol[4]);
            Console.WriteLine("Reduced column matrix is:");
            for (i = 0; i < a.GetLength(0); i++)
            {
                for (j = 0; j < a.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        a[i, j] = 100;
                    }
                    else
                    {
                        if (a[i, j] != 100)
                        {
                            a[i, j] = a[i, j] - arrmincol[j];
                        }
                    }
                    Console.Write(string.Format("{0}\t", a[i, j]));
                }
                Console.WriteLine();
            }
            for (i = 0; i < arrminrow.Length; i++)
            {
                cost += arrminrow[i] + arrmincol[i];
            }
            Console.WriteLine("The cost of reduced matrix is:{0}", cost);
            Console.WriteLine();
        }
        public int[,] reduction(int[,] a)
        {
            int count = 0;
            int start = 0;
            int newstart = 0;
            int i = start;
            int k = 0;
            int pos = 0;
            int pos1 = 0;
        label: if (count < a.GetLength(0))
            {
                min = a[i, 0];
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    if (a[i, j] < min)
                    {
                        min = a[i, j];
                        Console.WriteLine(min);
                        newstart = j;
                        Console.WriteLine(newstart);
                    }
                }
              
                p1[pos] = i;
                Console.WriteLine("the position of i is{0}", p1[pos++]);
                p2[pos1] = newstart;
                Console.WriteLine("the postion of j is{0}", p2[pos1++]);

                for (int j = 0; j < a.GetLength(1); j++)
                {
                    a[i, j] = 100;
                }

                for (int j = 0; j < a.GetLength(1); j++)
                {
                    a[newstart, i] = 100;
                    a[j, newstart] = 100;
                }
                for (int p = 0; p < a.GetLength(1); p++)
                {
                    for (int j = 0; j < a.GetLength(1); j++)
                    {
                        Console.Write(string.Format("{0}\t", a[p, j]));
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
                Console.WriteLine();
                i = newstart;
                count++;

                arrpos[i, k] = min;
            }
            //Console.WriteLine("path is{0}", a[0, 2]);
            if (count < a.GetLength(0))
            {
                goto label;
            }
            int sum = 0;

            for (int s = 0; s < p1.Length; s++)
            {
                sum = sum + a1[p1[s], p2[s]];

                Console.Write("( {0}", p1[s]);
                Console.Write(" , {0} )=>", p2[s]);
                Console.WriteLine();
               
            }
            Console.WriteLine("The minimum path is {0} ", sum);
            return a;
        }


    }
}
    //public void getvalue()
    //{
    //    for (int s = 0; s <arrmincol.Length; s++)
    //    {
    //     myarray.GetPosition[,]==a[2,5];
    //    }
    //}
