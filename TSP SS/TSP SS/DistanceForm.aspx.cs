﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TSP_SS
{
    public partial class DistanceForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
        }
    }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader reader=null;
            
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["salesDB"].ConnectionString);
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();

                }
                String query = "select Distance,Time from distance where City_Source=@source and City_Destination=@destiny";
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@source", txtSourceCity .Text );
                cmd.Parameters.AddWithValue("@destiny", txtDestinationCity.Text);
               reader=cmd.ExecuteReader();
               while (reader.Read())
               {
                   txtDistance.Text = reader["Distance"].ToString();
                   txtTime.Text = reader["Time"].ToString();
               }
            }
            catch (Exception ex)
            {
                Response .Write ("Error"+ex.Message );

            }


        }

        protected void txtTime_TextChanged(object sender, EventArgs e)
        {

        }
        }
}
   