﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DistanceForm.aspx.cs" Inherits="TSP_SS.DistanceForm" MasterPageFile="~/Site1.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table class="style1" align="center" >
        <tr>
            <td class="style2">
                Source City</td>
            <td>
                <asp:TextBox ID="txtSourceCity" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Destination City</td>
            <td>
                <asp:TextBox ID="txtDestinationCity" runat="server"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td class="style2">
                Distance(in Km)</td>
            <td>
                <asp:TextBox ID="txtDistance" runat="server"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td class="style2">
                Time(in Hour)</td>
            <td>
                <asp:TextBox ID="txtTime" runat="server" OnTextChanged="txtTime_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2" align="center" colspan="2">
                <asp:Button ID="btnCalculate" runat="server" onclick="btnCalculate_Click" 
                    Text="Calculate" Width="89px" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
   </asp:Content>