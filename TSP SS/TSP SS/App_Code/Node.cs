﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Node
/// </summary>
public class Node
{
    public Location StartPoint { get; set; }
    public Location EndPoint { get; set; }
    public Cost Cost { get; set; }
	public Node()
	{

	}

    internal Node GetLength(int p)
    {
        throw new NotImplementedException();
    }
}

public class Cost
{
    public decimal value { get; set; }
}

public class Location
{
    public int id { get; set; }
    public string name { get; set; }
}

public class CurrentMap
{
    
    public Node[,] get()
    {
        Location mumbai = new Location();
        mumbai.id = 1;
        mumbai.name = "mumbai";

        Location pune = new Location();
        pune.id = 2;
        pune.name = "pune";

        Location nagpur = new Location();
        nagpur.id = 3;
        nagpur.name = "nagpur";


        Location surat = new Location();
        surat.id = 4;
        surat.name = "surat";


        Location calcutta = new Location();
        calcutta.id = 5;
        calcutta.name = "calcutta";


        Location banglore = new Location();
        banglore.id = 6;
        banglore.name = "banglore";


        Location bidar = new Location();
        bidar.id = 7;
        bidar.name = "bidar";

        Node mumbai_pune = new Node() { StartPoint = mumbai, EndPoint = pune, Cost = new Cost() { value = 100} };

        Node mumbai_mumbai = new Node() { StartPoint = mumbai, EndPoint = pune, Cost = null };
        Node mumbai_nagpur = new Node() { StartPoint = mumbai, EndPoint = nagpur, Cost = new Cost() { value = 150 } };
        Node mumbai_surat = new Node() { StartPoint = mumbai, EndPoint = surat, Cost = new Cost() { value = 50 } };
        Node mumbai_calcutta = new Node() { StartPoint = mumbai, EndPoint = calcutta, Cost = new Cost() { value = 80 } };
        Node mumbai_banglore = new Node() { StartPoint = mumbai, EndPoint = banglore, Cost = new Cost() { value = 200 } };
        Node mumbai_bidar = new Node() { StartPoint = mumbai, EndPoint = bidar, Cost = new Cost() { value = 300 } };

        Node pune_pune = new Node() { StartPoint = pune, EndPoint = pune, Cost = null };
        Node pune_mumbai = new Node() { StartPoint = pune, EndPoint = mumbai, Cost = new Cost() { value = 100 } };
        Node pune_nagpur = new Node() { StartPoint = pune, EndPoint = nagpur, Cost = new Cost() { value = 50 } };
        Node pune_surat = new Node() { StartPoint = pune, EndPoint = surat, Cost = new Cost() { value = 70 } };
        Node pune_calcutta = new Node() { StartPoint = pune, EndPoint = calcutta, Cost = new Cost() { value = 200 } };
        Node pune_banglore = new Node() { StartPoint = pune, EndPoint = banglore, Cost = new Cost() { value = 90 } };
        Node pune_bidar = new Node() { StartPoint = pune, EndPoint = bidar, Cost = new Cost() { value = 250 } };

        Node nagpur_nagpur = new Node() { StartPoint = nagpur, EndPoint = nagpur, Cost = null };
        Node nagpur_pune = new Node() { StartPoint = nagpur, EndPoint = pune, Cost = new Cost() { value = 90 } };
        Node nagpur_mumbai = new Node() { StartPoint = nagpur, EndPoint = mumbai, Cost = new Cost() { value = 150 } };
        Node nagpur_surat = new Node() { StartPoint = nagpur, EndPoint = surat, Cost = new Cost() { value = 40 } };
        Node nagpur_calcutta = new Node() { StartPoint = nagpur, EndPoint = calcutta, Cost = new Cost() { value = 160 } };
        Node nagpur_banglore = new Node() { StartPoint = nagpur, EndPoint = banglore, Cost = new Cost() { value = 140 } };
        Node nagpur_bidar = new Node() { StartPoint = nagpur, EndPoint = bidar, Cost = new Cost() { value = 250 } };

        Node surat_surat = new Node() { StartPoint = surat, EndPoint = surat, Cost = null };
        Node surat_pune = new Node() { StartPoint = surat, EndPoint = pune, Cost = new Cost() { value = 50 } };
        Node surat_nagpur = new Node() { StartPoint = surat, EndPoint = nagpur, Cost = new Cost() { value = 40 } };
        Node surat_mumbai = new Node() { StartPoint = surat, EndPoint = mumbai, Cost = new Cost() { value = 50 } };
        Node surat_calcutta = new Node() { StartPoint = surat, EndPoint = calcutta, Cost = new Cost() { value = 130 } };
        Node surat_banglore = new Node() { StartPoint = surat, EndPoint = banglore, Cost = new Cost() { value = 120 } };
        Node surat_bidar = new Node() { StartPoint = surat, EndPoint = bidar, Cost = new Cost() { value = 80 } };

        Node calcutta_calcutta = new Node() { StartPoint = calcutta , EndPoint = calcutta , Cost = null };
        Node calcutta_pune = new Node() { StartPoint = calcutta, EndPoint = pune, Cost = new Cost() { value = 200 } };
        Node calcutta_nagpur = new Node() { StartPoint = calcutta, EndPoint = nagpur, Cost = new Cost() { value = 160 } };
        Node calcutta_surat = new Node() { StartPoint = calcutta, EndPoint = surat, Cost = new Cost() { value = 130 } };
        Node calcutta_mumbai = new Node() { StartPoint = calcutta, EndPoint = mumbai, Cost = new Cost() { value = 80 } };
        Node calcutta_banglore = new Node() { StartPoint = calcutta, EndPoint = banglore, Cost = new Cost() { value = 200 } };
        Node calcutta_bidar = new Node() { StartPoint = calcutta, EndPoint = bidar, Cost = new Cost() { value = 100 } };

        Node banglore_banglore = new Node() { StartPoint = banglore, EndPoint = banglore, Cost = null };
        Node banglore_pune = new Node() { StartPoint = banglore, EndPoint = pune, Cost = new Cost() { value = 90 } };
        Node banglore_nagpur = new Node() { StartPoint = banglore, EndPoint = nagpur, Cost = new Cost() { value = 140 } };
        Node banglore_surat = new Node() { StartPoint = banglore, EndPoint = surat, Cost = new Cost() { value = 120 } };
        Node banglore_calcutta = new Node() { StartPoint = banglore, EndPoint = calcutta, Cost = new Cost() { value = 200 } };
        Node banglore_mumbai = new Node() { StartPoint = banglore, EndPoint = mumbai, Cost = new Cost() { value = 200 } };
        Node banglore_bidar = new Node() { StartPoint = banglore, EndPoint = bidar, Cost = new Cost() { value = 100 } };

        Node bidar_bidar = new Node() { StartPoint = bidar, EndPoint = bidar, Cost = new Cost() { value = 150 } };
        Node bidar_pune = new Node() { StartPoint = bidar, EndPoint = pune, Cost = new Cost() { value = 250 } };
        Node bidar_nagpur = new Node() { StartPoint = bidar, EndPoint = nagpur, Cost = new Cost() { value = 250 } };
        Node bidar_surat = new Node() { StartPoint = bidar, EndPoint = surat, Cost = new Cost() { value = 80 } };
        Node bidar_calcutta = new Node() { StartPoint = bidar, EndPoint = calcutta, Cost = new Cost() { value = 90 } };
        Node bidar_banglore = new Node() { StartPoint = bidar, EndPoint = banglore, Cost = new Cost() { value = 180 } };
        Node bidar_mumbai = new Node() { StartPoint = bidar, EndPoint = mumbai, Cost = new Cost() { value = 300 } };

        Node[,] map = new Node [7, 7] {
        {mumbai_mumbai, mumbai_pune,mumbai_nagpur ,mumbai_surat,mumbai_calcutta, mumbai_banglore, mumbai_bidar },
        {pune_mumbai, pune_pune,pune_nagpur ,pune_surat,pune_calcutta, pune_banglore, pune_bidar },
        {nagpur_mumbai, nagpur_pune,nagpur_nagpur ,nagpur_surat,nagpur_calcutta,nagpur_banglore,nagpur_bidar },
        {surat_mumbai, surat_pune,surat_nagpur ,surat_surat,surat_calcutta, surat_banglore, surat_bidar },
         {calcutta_mumbai, calcutta_pune,calcutta_nagpur ,calcutta_surat,calcutta_calcutta, calcutta_banglore, calcutta_bidar },
          {banglore_mumbai, banglore_pune,banglore_nagpur ,banglore_surat,banglore_calcutta, banglore_banglore, banglore_bidar },
          {bidar_mumbai, bidar_pune,bidar_nagpur ,bidar_surat,bidar_calcutta, bidar_banglore, bidar_bidar },
        
        };

        return map;
    }
}

public class ShortDistanceCalculator
{

    List<CurrentMap> maps { get; set; }

    public decimal GetValue()
    {
        
        CurrentMap map = new CurrentMap();
        foreach (var node in map.get())
	{
        
	}

       
        Console.ReadLine();
        {

        }
    }
}