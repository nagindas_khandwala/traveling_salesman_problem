﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TSP_SS
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
          
            SqlConnection conn = null;
            SqlCommand cmd = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["salesDB"].ConnectionString);
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();

                }
                String query = "select count(*) from saleslogin where username=@username and password=@password";
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@username", txtUserID .Text);
                cmd.Parameters.AddWithValue("@password", txtPassword .Text );
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                if (count == 1)
                {
                    Session["username"] = txtUserID.Text;
                    Response.Redirect("DistanceForm.aspx");
                }
                else
                {

                    Response.Write("Invalid uername or password");
                }
            }
            catch (Exception ex)
            {
               

            }

            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(typeof (Login),"closePage","window.close();",true);
        }
    }
}