﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TSP_SS
{
    public partial class Registration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RegisterUser(object sender, EventArgs e)
        {
            SqlCommand com = null;
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["salesDB"].ConnectionString);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();

                }

                String query = "insert into saleslogin values (@name,@pass,@email)";
                com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@name", txtUsername .Text);
                com.Parameters.AddWithValue("@pass", txtPassword .Text );
                com.Parameters.AddWithValue("@email", txtEmail.Text);

                com.ExecuteNonQuery();
                /*if (count == 1)
                {
                    Session["username"] = txtUsername.Text;
                    Response.Redirect("Default.aspx");


                }
                else
                {

                    Response.Write("Invalid uername or password");
                }*/
            }
            catch (Exception ex)
            {

                Response.Write("Error " + ex.Message);
            }
            Response.Redirect("Login.aspx");
        }
    }
}