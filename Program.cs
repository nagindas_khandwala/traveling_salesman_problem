﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Programs
{
    
    static void Main(string[] args)
    {
        
        int[,] a = new int[5, 5] { { 100, 15, 6, 10, 9 }, { 15, 100, 7, 5, 8 }, { 6, 7, 100, 7, 9 }, { 10, 5, 7, 100, 11 }, { 9, 8, 9, 11, 100 } };
        int  min1,min2,min3,min4,min5,min6,min7,min8=0;
        int[,] r1 = new int [5,5];
        int[,] r2 = new int[5, 5];
        int i;
        
        int cost;
        int j;
        Console.WriteLine("Actual matrix");
        Console.WriteLine();
       
        for (i = 0; i < a.GetLength(0); i++)
        {
            for (j = 0; j < a.GetLength(1); j++)
            {
                Console.Write(string.Format("{0}\t", a[i, j]));
            }
                 Console.WriteLine();
        }

        

        //for (int i = 0; i < a.GetLength(0); i++)
        //{
        i = 0;
            min1 =(int)a[0,0];
            for (j = 0; j < a.GetLength(1); j++)
            {
                if (a[i,j]<min1)
                {
                           min1 = (int)a[0, j];
                   
                }
              }
           
        //}
        Console.WriteLine(" min value is {0}",min1);

        Console.WriteLine("1st row reduction is:");
        r1[0, 0] = 100;
         r1[0,1]=a[0,1]-min1;
         r1[0,2]=a[0,2]-min1;
         r1[0,3]=a[0,3]-min1;
         r1[0,4]=a[0,4]-min1;
         Console.WriteLine("{0}", r1[0, 1]);
         Console.WriteLine("{0}", r1[0, 2]);
         Console.WriteLine("{0}", r1[0, 3]);
         Console.WriteLine("{0}", r1[0, 4]);


         //for (int i = 0; i < a.GetLength(0); i++)
         //{
         i = 1;
         min2 = (int)a[0, 1];
         for (j = 0; j < a.GetLength(1); j++)
         {
             if (a[i, j] < min2)
             {
                 min2 = (int)a[1, j];

             }
         }

         //}
         Console.WriteLine(" min value is {0}", min2);

         Console.WriteLine("2nd row reduction is:");
         r1[1, 1] = 100;
         r1[1, 0] = a[1, 0] - min2;
         r1[1, 2] = a[1, 2] - min2;
         r1[1, 3] = a[1, 3] - min2;
         r1[1, 4] = a[1, 4] - min2;
         Console.WriteLine("{0}", r1[1, 0]);
         Console.WriteLine("{0}", r1[1, 2]);
         Console.WriteLine("{0}", r1[1, 3]);
         Console.WriteLine("{0}", r1[1, 4]);


         //for (int i = 0; i < a.GetLength(0); i++)
         //{
         i = 2;
         min3 = (int)a[0, 2];
         for (j = 0; j < a.GetLength(1); j++)
         {
             if (a[i, j] < min3)
             {
                 min3 = (int)a[2, j];

             }
         }

         //}
         Console.WriteLine(" min value is {0}", min3);

         Console.WriteLine("3rd row reduction is:");
         r1[2, 0] = a[2, 0] - min3;
         r1[2, 2] = 100;
         r1[2, 1] = a[2, 1] - min3;
         r1[2, 3] = a[2, 3] - min3;
         r1[2, 4] = a[2, 4] - min3;
         Console.WriteLine("{0}", r1[2, 0]);
         Console.WriteLine("{0}", r1[2, 1]);
         Console.WriteLine("{0}", r1[2, 3]);
         Console.WriteLine("{0}", r1[2, 4]);


         //for (int i = 0; i < a.GetLength(0); i++)
         //{
         i = 3;
         min4 = (int)a[0, 3];
         for (j = 0; j < a.GetLength(1); j++)
         {
             if (a[i, j] < min4)
             {
                 min4 = (int)a[3, j];

             }
         }

         //}
         Console.WriteLine(" min value is {0}", min4);

         Console.WriteLine("4th row reduction is:");
         r1[3, 0] = a[3, 0] - min4;
         r1[3, 1] = a[3, 1] - min4;
         r1[3, 2] = a[3, 2] - min4;
         r1[3, 3] = 100;
         r1[3, 4] = a[3, 4] - min4;
         Console.WriteLine("{0}", r1[3, 0]);
         Console.WriteLine("{0}", r1[3, 1]);
         Console.WriteLine("{0}", r1[3, 2]);
         Console.WriteLine("{0}", r1[3, 4]);



         //for (int i = 0; i < a.GetLength(0); i++)
         //{
         i = 4;
         min5 = (int)a[0, 4];
         for (j = 0; j < a.GetLength(1); j++)
         {
             if (a[i, j] < min5)
             {
                 min5 = (int)a[4, j];

             }
         }

         //}
         Console.WriteLine(" min value is {0}", min5);

         Console.WriteLine("5th row reduction is:");
         r1[4, 0] = a[4, 0] - min5;
         r1[4, 1] = a[4, 1] - min5;
         r1[4, 2] = a[4, 2] - min5;
         r1[4, 3] = a[4,3] - min5;
         r1[4, 4] = 100;
         Console.WriteLine("{0}", r1[4, 0]);
         Console.WriteLine("{0}", r1[4, 1]);
         Console.WriteLine("{0}", r1[4, 2]);
         Console.WriteLine("{0}", r1[4, 3]);

         Console.WriteLine("Reduced row matrix is:");

         for (i = 0; i < r1.GetLength(0); i++)
         {
             for (j = 0; j < r1.GetLength(1); j++)
             {
                 Console.Write(string.Format("{0}\t", r1[i, j]));
             }
             Console.WriteLine();
         }


         //for (int i = 0; i < a.GetLength(0); i++)
         //{
       j = 4;
         min6 = (int)r1[0, 4];
         for ( i= 0; i < r1.GetLength(1); i++)
         {
             if (r1[i, j] < min6)
             {
                 min6 = (int)r1[4, i];

             }
         }

         //}
         Console.WriteLine(" min value is {0}", min6);

         Console.WriteLine("4th coloumn reduction is:");

         r1[0, 4] = r1[0, 4] - min6;
         r1[1, 4] = r1[1, 4] - min6;
         r1[2, 4] = r1[2, 4] - min6;
         r1[3, 4] = r1[3, 4] - min6;
         r1[4, 4] = 100;
         Console.WriteLine("{0}", r1[0, 4]);
         Console.WriteLine("{0}", r1[1, 4]);
         Console.WriteLine("{0}", r1[2, 4]);
         Console.WriteLine("{0}", r1[3, 4]);

         Console.WriteLine("Reduced column matrix is:");

         for (i = 0; i < r1.GetLength(0); i++)
         {
             for (j = 0; j < r1.GetLength(1); j++)
             {
                 Console.Write(string.Format("{0}\t", r1[i, j]));
             }
             Console.WriteLine();
         }
         cost = min1+min2+min3+min4+min5+min6;
         Console.WriteLine("The cost of matrix is:{0}", cost);

        
         for (i = 0; i < 1; i++)
         {
             for (j = 0; j < r1.GetLength(1); j++)
             {
                 r1[i, j] = 100;
               
             }

         }

         for (i = 0; i < r1.GetLength(0); i++)
         {
             for (j = 1; j < 2; j++)
             {
                 r1[i, j] = 100;
                 r1[1, 0] = 100;
             }
             
         }

         //System.Console.WriteLine(int.MaxValue);

         Console.WriteLine("to give largest number to 1st row and 2nd column");
         

         for (i = 0; i < r1.GetLength(0); i++)
         {
             for (j = 0; j < r1.GetLength(1); j++)
             {
                 Console.Write(string.Format("{0}\t", r1[i, j]));
             }
             Console.WriteLine();
         }


         //for (int i = 0; i < a.GetLength(0); i++)
         //{
         i = 3;
         min7 = (int)r1[0, 3];
         for (j = 0; j < r1.GetLength(1); j++)
         {
             if (r1[i, j] < min7)
             {
                 min7 = (int)r1[3, j];

             }
         }

         //}
         Console.WriteLine(" min value is {0}", min7);

         Console.WriteLine("4th row reduction is:");
         r2[3, 0] = r1[3, 0] - min7;
         r2[3, 2] = r1[3, 2] - min7;
         r2[3, 4] = r1[3, 4] - min7;
         Console.WriteLine("{0}", r2[3, 0]);
  
         Console.WriteLine("{0}", r2[3, 2]);
         Console.WriteLine("{0}", r2[3, 4]);



         //for (int i = 0; i < a.GetLength(0); i++)
         //{
         i = 4;
         min8 = (int)r1[0, 4];
         for (j = 0; j < r1.GetLength(1); j++)
         {
             if (r1[i, j] < min8)
             {
                 min8 = (int)r1[4, j];

             }
         }

         //}
         Console.WriteLine(" min value is {0}", min8);

         Console.WriteLine("5th row reduction is:");
         r2[4, 0] = r1[4, 0] - min8;
         r2[4, 2] = r1[4, 2] - min8;
         r2[4, 3] = r1[4, 3] - min8;
        
         Console.WriteLine("{0}", r2[4, 0]);

         Console.WriteLine("{0}", r2[4, 2]);
         Console.WriteLine("{0}", r2[4, 3]);

         Console.WriteLine("Reduced new row matrix is:");

         for (i = 0; i < r2.GetLength(0); i++)
         {
             for (j = 0; j < r2.GetLength(1); j++)
             {
                 Console.Write(string.Format("{0}\t", r2[i, j]));
             }
             Console.WriteLine();
         }


        Console.ReadKey();
  
    }
}


      
